//
//  PCReportsMainInteractor.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.
//
//

import UIKit

protocol PCReportsMainBusinessLogic {
    func generateReportList(filter:Bool)
}

protocol PCReportsMainDataStore {
    var reportList:[PCReport] { get }
}

class PCReportsMainInteractor: PCReportsMainBusinessLogic, PCReportsMainDataStore {
    var reportList: [PCReport] = [PCReport]() {
        didSet {
            self.presenter?.handleReportList(list: reportList)
        }
    }
    
    var presenter: PCReportsMainPresentationLogic?
    var worker: PCReportsMainWorker?
    
    // MARK: PCReportsMainBusinessLogic
    
    func generateReportList(filter:Bool = false) {
        worker = PCReportsMainWorker()
        worker?.getTransportList()
        if filter {
            //filter items with premissions
        } else {
            if let reports = getReportsFromPlist() {
                reportList = reports
            } else {
                print("Error in Reports.plist")
            }
        }
    }
    
    private func getReportsFromPlist() -> [PCReport]? {
        if let path = Bundle.main.path(forResource: "Reports", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                do {
                    let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    print("")
                    if let reportListData = data.parseCodableObject(structType: PCReportList.self) {
                        return Array(reportListData.values)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        return nil
    }

    
}
