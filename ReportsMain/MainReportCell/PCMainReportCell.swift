//
//  PCMainReportCell.swift
//  Pcounter
//
//  Created by alexandr shitikov on 19.02.2020.
//  Copyright © 2020 Intranso. All rights reserved.
//

import UIKit

class PCMainReportCell: PCTableViewCell {
    
    @IBOutlet weak var reportIcon: UIImageView!
    @IBOutlet weak var reportTitle: UILabel!
    @IBOutlet weak var reportDescription: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    override func configureUi() {
        super.configureUi()
        cardView.rounded(radius: 13)
        cardView.clipsToBounds = true
        cardView.backgroundColor = UIColor.darkBackground
        reportTitle.textColor = UIColor.mainText
        reportDescription.textColor = UIColor.secondaryText
    }
    
    func configure(item: PCReportsMain.ViewModel) {
        checkAccess(access: item.isVisible)
        reportTitle.text = item.title
        reportDescription.text = item.subTitle
        reportIcon.image = UIImage(named: item.iconName)
    }
    
    private func checkAccess(access:Bool) {
        if access {
            cardView.alpha = 1.0
        } else {
            cardView.alpha = 0.6
        }
    }
    
}
