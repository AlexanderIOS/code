//
//  PCReportsMainPresenter.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.


import UIKit

protocol PCReportsMainPresentationLogic {
    func handleReportList(list:[PCReport])
    func handleError(error:PCResponseError?)
}

class PCReportsMainPresenter: PCReportsMainPresentationLogic

{
    weak var viewController: PCReportsMainDisplayLogic?
    
    // MARK: PCReportsMainPresentationLogic
    
    func handleReportList(list:[PCReport]) {
        var viewModelList = [PCReportsMain.ViewModel]()
        list.forEach { (report) in
            let access = PCDataHandler.shared.havePermission(type: report.serverType)
            let viewModel = PCReportsMain.ViewModel(title: report.localizedTitle.localized(),
                                                    subTitle: report.localizedDescription.localized(),
                                                    iconName: report.iconName,
                                                    index: report.index,
                                                    isVisible: access,
                                                    reportType: report.type)
            viewModelList.append(viewModel)
        }
        viewController?.handleReportList(list: viewModelList.sorted(by: {$0.index < $1.index}))
    }
    
    func handleError(error:PCResponseError?) {
        #warning("implementation requires changes on the server PC-1039")
    }
    
    
}
