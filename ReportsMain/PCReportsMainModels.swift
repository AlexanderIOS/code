//
//  PCReportsMainModels.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.


import UIKit

enum PCReportType: CaseIterable {
      case expressReport
      case passengerFlow
  }

enum PCReportsMain {
  // MARK: Use cases
    
    struct ViewModel {
        let title:String
        let subTitle:String
        let iconName:String
        let index:Int
        var isVisible:Bool
        let reportType:String
    }
    
}

// MARK: Reports
typealias PCReportList = [String:PCReport]

struct PCReport: Codable {
    let iconName: String
    let localizedTitle: String
    let localizedDescription: String
    let type: String
    let index: Int
    let serverType:String

    enum CodingKeys: String, CodingKey {
        case iconName = "iconName"
        case localizedTitle = "localizedTitle"
        case localizedDescription = "localizedDescription"
        case type = "type"
        case index = "index"
        case serverType = "serverTypeForAccess"
    }
}

// MARK: - Transport

typealias PCTransportList = [PCTransport]

struct PCTransport: Codable {
    let id: String
    let displayName: String
    let transportType: String
    let colorInMap: String?
    let carrierId: String
    let on: Bool?
    let disabled: Bool?
    let timeBetweenStops: Int?
    let stopsTime: Int?
    let priceForKm: Int?
    let minPrice: Int?
    let pathBetweenStops: Int?
    let ticketDeviceId: String?
    let workPeriod: PCWorkPeriod?
    let routeNumber: String?
    let priveleges: Int?
    let engine: PCEngine?
    let group: String?
    let passengerCapacity: Int?
    let passengerOnline: Int?
    let numberTransport: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case displayName = "displayName"
        case transportType = "transportType"
        case colorInMap = "colorInMap"
        case carrierId = "carrierId"
        case on = "on"
        case disabled = "disabled"
        case timeBetweenStops = "timeBetweenStops"
        case stopsTime = "stopsTime"
        case priceForKm = "priceForKm"
        case minPrice = "minPrice"
        case pathBetweenStops = "pathBetweenStops"
        case ticketDeviceId = "ticketDeviceId"
        case workPeriod = "workPeriod"
        case routeNumber = "routeNumber"
        case priveleges = "priveleges"
        case engine = "engine"
        case group = "group"
        case passengerCapacity = "passengerCapacity"
        case passengerOnline = "passengerOnline"
        case numberTransport = "numberTransport"
    }
}

// MARK: - PCEngine
struct PCEngine: Codable {
    let fuelTankVolume: Double?
    let fuelType: String?
    let fuelConsumption: Double?
    let summerSeason: Double?
    let winterSeason: Double?
    let cityCoefficient: Double?
    let outsideCityCoefficient: Double?
    let oilConsumption: Double?
    let lastOilChange: String?
    let distanceAfterOilChange: Double?

    enum CodingKeys: String, CodingKey {
        case fuelTankVolume = "fuelTankVolume"
        case fuelType = "fuelType"
        case fuelConsumption = "fuelConsumption"
        case summerSeason = "summerSeason"
        case winterSeason = "winterSeason"
        case cityCoefficient = "cityCoefficient"
        case outsideCityCoefficient = "outsideCityCoefficient"
        case oilConsumption = "oilConsumption"
        case lastOilChange = "lastOilChange"
        case distanceAfterOilChange = "distanceAfterOilChange"
    }
}

// MARK: - PCWorkPeriod
struct PCWorkPeriod: Codable {
    let start: String?
    let finish: String?

    enum CodingKeys: String, CodingKey {
        case start = "start"
        case finish = "finish"
    }
}
