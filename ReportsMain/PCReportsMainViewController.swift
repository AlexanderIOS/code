//
//  PCReportsMainViewController.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.


import UIKit

protocol PCReportsMainDisplayLogic: class {
    func handleReportList(list:[PCReportsMain.ViewModel])
    func handleError(message:String)
}

class PCReportsMainViewController: PCViewController, PCReportsMainDisplayLogic
{
    var interactor: PCReportsMainBusinessLogic?
    var router: (NSObjectProtocol & PCReportsMainRoutingLogic & PCReportsMainDataPassing)?
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataArray = [PCReportsMain.ViewModel]()
    let cellId = "PCMainReportCell"
    let heightForRow:CGFloat = 180
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = PCReportsMainInteractor()
        let presenter = PCReportsMainPresenter()
        let router = PCReportsMainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        title = String.lc_reportsTabBarItemTitle
    }
    
    override func configureUi() {
        super.configureUi()
        let nib = UINib(nibName: cellId, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
        setBackgroundImage(image: nil)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        view.backgroundColor = UIColor.darkBackground
        tableView.delegate = self
        tableView.dataSource = self
        navigationController?.setStyle(style: .darkBlue, largeTitle: false)
    }
    
    // MARK: View lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor?.generateReportList(filter: false)
    }
    
    
}
