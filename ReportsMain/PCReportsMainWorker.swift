//
//  PCReportsMainWorker.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.

import UIKit

class PCReportsMainWorker {
    
    /**get all  avalible transport from server*/
    func getTransportList(completion: (([PCTransport]?) -> ())? = nil) {
        let url = PCURLs.Transport.carrier.rawValue
        PCAPIManager.manager.performRequestWith(method: .POST, url: url, params: nil, showLoader: false, completion: { (data, response, error) in
            if let transportList = data?.parseCodableObject(structType: PCTransportList.self) {
                PCDataHandler.shared.saveTransportList(list: transportList)
                completion?(transportList)
            } else {
                completion?(nil)
            }
        })
    }
  
}
