//
//  PCReportsMainRouter.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.

import UIKit

@objc protocol PCReportsMainRoutingLogic {
    func navigateToExpressReport()
    func navigateToPassengerReport()
}

protocol PCReportsMainDataPassing
{
    var dataStore: PCReportsMainDataStore? { get }
}

class PCReportsMainRouter: NSObject, PCReportsMainRoutingLogic, PCReportsMainDataPassing
{
    weak var viewController: PCReportsMainViewController?
    var dataStore: PCReportsMainDataStore?
    let expressReportControllerId = "PCExpressReportMainViewController"
    let passengerReportControllerId = "PCPassengerFlowMainViewController"
    
    // MARK: Routing
    
    func navigateToExpressReport() {
        let controller = PCStoryboard.ExpressReportMain.instance.instantiateViewController(withIdentifier: expressReportControllerId)
        self.viewController?.navigationController?.setStyle(style: .darkBlue, largeTitle: false)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func navigateToPassengerReport() {
        let controller = PCStoryboard.PassengerFlowMain.instance.instantiateViewController(withIdentifier: passengerReportControllerId)
        self.viewController?.navigationController?.setStyle(style: .darkBlue, largeTitle: false)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
}
