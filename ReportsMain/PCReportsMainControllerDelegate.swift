//
//  PCReportsMainControllerDelegate.swift
//  Pcounter
//
//  Created by alexandr shitikov on 13.02.2020.
//  Copyright (c) 2020 Intranso. All rights reserved.

import UIKit

extension PCReportsMainViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: PCReportsMainDisplayLogic
    func handleReportList(list:[PCReportsMain.ViewModel]) {
        dataArray.removeAll()
        dataArray = list
        tableView.reloadData()
    }
    
    func handleError(message:String) {
        #warning("implementation requires changes on the server PC-1039")
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! PCMainReportCell
        let item = dataArray[indexPath.item]
        cell.configure(item: item)
        return cell
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = dataArray[indexPath.item]
        if item.isVisible {
            switch item.index { // see report indexes in Report.plist
            case 0:
                router?.navigateToExpressReport()
            case 1:
                router?.navigateToPassengerReport()
            default:
                print("no case in raport main")
            }
        } else {
            accessDeniedAlertAction()
        }
        
        
    }
    
    func accessDeniedAlertAction() {
        UIAlertController.alert(title: String.lc_alertAccessDeniedTitle,
                                message: String.lc_alertAccessDeniedDescription,
                                action: nil,
                                target: self)
    }
    
    
    
}
